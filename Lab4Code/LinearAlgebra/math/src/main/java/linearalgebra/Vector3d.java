//Mohamed Aljak
//SID: 2237281

package linearalgebra;

public class Vector3d 
{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        double magnitude = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        return magnitude;
    }

    public double dotProduct(Vector3d othVector3d){
        double xProduct = this.x * othVector3d.x;
        double yProduct = this.y * othVector3d.y;
        double zProduct = this.z * othVector3d.z;

        double dotProduct = xProduct + yProduct + zProduct;
        return dotProduct;
    }

    public Vector3d add(Vector3d oVector3d){
        double xSum = this.x + oVector3d.x;
        double ySum = this.y + oVector3d.y;
        double zSum = this.z + oVector3d.z;

        Vector3d newVector3d = new Vector3d(xSum, ySum, zSum);
        return newVector3d;
    }

    public boolean equals(Object o){
        if(this ==  o){
            return true;
        }

        if(!(o instanceof Vector3d)){
            return false;
        }

        Vector3d comparedVector3d = (Vector3d) o;

        if(this.x == comparedVector3d.x &&
            this.y == comparedVector3d.y &&
            this.z == comparedVector3d.z){
                return true;
        }

        return false;
    }
}
