//Mohamed Aljak
//SID: 2237281

package linearalgebra;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class Vector3dTest 
{
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void gettersTest(){
        Vector3d vector3d = new Vector3d(3, 5, 7);
        assertEquals( "Getter getX",3, vector3d.getX(),0);
        assertEquals("Getter getY", 5, vector3d.getY(), 0);
    }

    @Test
    public void magnitudeTest(){
        Vector3d vector3d = new Vector3d(3, 5, 7);
        assertEquals("Magnitude", Math.sqrt(83), vector3d.magnitude(), 0);
    }

    @Test 
    public void dotProductTest(){
        Vector3d vector3d = new Vector3d(3, 5, 7);
        Vector3d ovector3d = new Vector3d(4, 6, 8);

        assertEquals("Dot product", 98, vector3d.dotProduct(ovector3d), 0);
    }

    @Test
    public void addTest(){
        Vector3d vector3d = new Vector3d(3, 5, 7);
        Vector3d ovector3d = new Vector3d(4, 6, 8);

        Vector3d expectedVector = new Vector3d(7, 11, 15);
        Vector3d actualVectorResult = vector3d.add(ovector3d);

        assertEquals("Add Vectors", expectedVector, actualVectorResult);
    }
}
